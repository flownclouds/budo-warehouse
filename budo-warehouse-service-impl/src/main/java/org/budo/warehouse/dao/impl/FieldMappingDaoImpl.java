package org.budo.warehouse.dao.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.mybatis.dao.MybatisDao;
import org.budo.support.dao.page.Page;
import org.budo.support.lang.util.MapUtil;
import org.budo.warehouse.dao.api.IFieldMappingDao;
import org.budo.warehouse.service.entity.FieldMapping;
import org.springframework.stereotype.Repository;

/**
 * @author lmw
 */
@Repository
public class FieldMappingDaoImpl implements IFieldMappingDao {
    @Resource
    private MybatisDao mybatisDao;

    @Override
    public List<FieldMapping> listByPipelineId(Integer pipelineId) {
        String sql = " SELECT * FROM t_field_mapping WHERE pipeline_id=#{pipelineId} AND (deleted_at IS NULL OR deleted_at='') ";
        Map<String, Object> parameter = MapUtil.stringObjectMap("pipelineId", pipelineId);
        return mybatisDao.listBySql(FieldMapping.class, sql, parameter, Page.max());
    }
}