package org.budo.warehouse.logic.api;

/**
 * @author limingwei
 */
public interface DataConsumer {
    void consume(DataEntry dataEntry);
}