package org.budo.warehouse.logic.consumer.jdbc;

import java.util.List;

import org.budo.support.lang.util.ListUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.warehouse.logic.api.DataEntry;

/**
 * @author lmw
 */
public class MysqlDataConsumer extends JdbcDataConsumer {
    /**
     * 追加 ON DUPLICATE KEY UPDATE 逻辑
     */
    @Override
    protected SqlUnit insertRow(DataEntry dataEntry, int rowIndex) {
        SqlUnit insertRow = super.insertRow(dataEntry, rowIndex);
        List<Object> parameters = ListUtil.arrayToList(insertRow.getParameters());

        List<String> set = this.set(dataEntry, rowIndex, parameters);
        String sql = insertRow.getSql() + " ON DUPLICATE KEY UPDATE " + StringUtil.join(set, ", ");

        return new SqlUnit(sql, parameters.toArray());
    }
}