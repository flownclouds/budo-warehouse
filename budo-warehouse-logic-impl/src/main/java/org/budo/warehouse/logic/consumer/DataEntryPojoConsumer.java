package org.budo.warehouse.logic.consumer;

import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.service.entity.Pipeline;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataEntryPojoConsumer implements DataConsumer {
    private DataConsumer dataConsumer;

    private Pipeline pipeline;

    @Override
    public void consume(DataEntry dataEntry) {
        DataEntryPojo dataEntryPojo = new DataEntryPojo(dataEntry); // 序列化传输

        dataEntryPojo.setTableName(pipeline.getTargetTable()); // 供下一步匹配
        dataEntryPojo.setSchemaName(pipeline.getTargetSchema());
        dataEntryPojo.setSourceDataNodeId(pipeline.getTargetDataNodeId());

        dataConsumer.consume(dataEntryPojo);
    }
}