package org.budo.warehouse.logic.producer;

import javax.annotation.Resource;

import org.budo.canal.message.handler.DefaultCanalMessageHandler;
import org.budo.warehouse.logic.api.DataConsumer;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;

import lombok.Getter;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
public class WarehouseCanalMessageHandler extends DefaultCanalMessageHandler {
    @Resource(name = "dispatcherDataConsumer")
    private DataConsumer dataConsumer;

    private Integer dataNodeId;

    @Override
    protected void handleCanalEntry(Entry entry) {
        CanalDataEntry canalDataEntry = new CanalDataEntry(entry, this.getDataNodeId());
        dataConsumer.consume(canalDataEntry);
    }
}