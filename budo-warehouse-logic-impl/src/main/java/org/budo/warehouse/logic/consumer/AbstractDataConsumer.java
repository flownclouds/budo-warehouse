package org.budo.warehouse.logic.consumer;

import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.service.entity.Pipeline;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Getter
@Setter
@Slf4j
public class AbstractDataConsumer implements DataConsumer {
    private Pipeline pipeline;

    private DataNode dataNode;

    @Override
    public void consume(DataEntry dataEntry) {
        log.info("#15 dataEntry=" + dataEntry);
    }
}