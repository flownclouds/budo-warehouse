package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 一个数据节点,数据库或kafka等
 * 
 * @author limingwei
 */
@Getter
@Setter
@Table(name = "t_datanode")
public class DataNode implements Serializable {
    private static final long serialVersionUID = 5836080966316075658L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String url;

    @Column
    private String username;

    @Column
    private String password;

    /**
     * 是否已删除，未删除时为空
     */
    @Column(name = "deleted_at")
    private Timestamp deletedAt;

    @Override
    public String toString() {
        return super.toString() + "[id=" + id + ", url=" + url + ", username=" + username + "]";
    }
}